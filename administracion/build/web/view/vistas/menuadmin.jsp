<%-- 
    Document   : menuadmin
    Created on : 26/11/2017, 01:40:32 AM
    Author     : KAK-VEG12
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>menuadmin</title>
    </head>+
    <body>
        <h1>Bienvenido Administrador ${user.username}</h1>
        <h4>MENU</h4>
        <nav>
            | <a href="create_laboratory.jsp">CREAR LABORATORIO</a> | </br>
            | <a href="update_laboratory.jsp">CAMBIAR DATOS DE LABORATORIO</a> | </br>
            | <a href="consulta_material_admin">CONSULTAR MATERIAL DE LABORATORIO</a> | </br>
            | <a href="alta_material.jsp">ALTAS Y ASIGNACION DE MATERIAL A LABORATORIO</a> | </br>
            | <a href="update_material.jsp">CAMBIOS AL MATERIAL DE LABORATORIO</a> | </br>
            | <a href="baja_material.jsp">BAJAS DE MATERIAL DE LABORATORIO</a> | </br>
            | <a href="consul_gral.jsp">CONSULTAR MATERIAL DE TODOS LOS LABORATORIOS</a> | </br>
            | <a href="create_user.html">CREAR NUEVOS USUARIOS</a> | </br>
            | <a href="logout">Log out</a> |
        </nav><br/>
</body>
</html>
