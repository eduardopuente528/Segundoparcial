<%-- 
    Document   : update_material
    Created on : 28/11/2017, 08:05:35 AM
    Author     : Lalo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Editar Material</h1>
        <form action="controller" method="POST">
            Id <input name="id"/><br/>
            Descripcion <input name="descripcion" /><br/>
            Cantidad <input name="cantidad" /><br/>
            Laboratorio <input name="labo" /><br/>
            <button>Editar</button>
            <input name="action" type="hidden" value="update" />
        </form>
    </body>
</html>
