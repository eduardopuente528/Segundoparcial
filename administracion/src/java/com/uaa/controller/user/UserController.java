/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.controller.user;

import com.uaa.entity.Usuario;
import com.uaa.helper.JBUserHelper;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lalo
 */
@WebServlet(name = "UserController", urlPatterns = {"/create_user"})
public class UserController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String view = "/create_user.html";
        RequestDispatcher rd = request.getRequestDispatcher(view);
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String view = "/create_user.html";
        JBUserHelper userHelper = JBUserHelper.getInstance();
        //obtener parametros de la forma
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        //boolean isadmin = Boolean.parseBoolean(request.getParameter("isadmin"));
        boolean isadmin;
        if (request.getParameter("isadmin") == null) {
            isadmin = false;
        } else {
            isadmin = true;
        }

        //crear una nueva instancia de Usuario para utilizarse en JPA
        Usuario user = new Usuario();
        user.setUsername(username);
        user.setPassword(password);
        user.setIsadmin(isadmin);
        //usar em metodo registerUser del helper para crear un nuevo registro
        userHelper.registerUser(user);

        //redirigir a la vista create_user.html
        RequestDispatcher rd = request.getRequestDispatcher(view);
        rd.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
