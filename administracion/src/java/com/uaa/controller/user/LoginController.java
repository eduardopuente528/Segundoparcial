/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.controller.user;

import com.uaa.entity.Usuario;
import com.uaa.helper.JBUserHelper;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Lalo
 */
@WebServlet(name = "LoginController", urlPatterns = {"/login"})
public class LoginController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        //si ya existe una session y ya se encuentra un usuario registrado entonces se redirige a inbox
        if (session != null && session.getAttribute("user") != null) {
            response.sendRedirect("menuadmin");
        } else {
            //si no existe una session ni un usuario registrado entonces cargamos la vista de login
            String view = "/login.jsp";
            RequestDispatcher rd = request.getRequestDispatcher(view);
            rd.forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        boolean pass = false;
        String errorMsg = "Credenciales Invalidas";

        JBUserHelper userHelper = JBUserHelper.getInstance();
        List<Usuario> userList = userHelper.getUserByUsername(username);

        //usuario no encontrado
        if (userList == null || userList.size() == 0) {
            RequestDispatcher rd = request.getRequestDispatcher("/login.jsp");
            request.setAttribute("error", errorMsg);
            rd.forward(request, response);
        } else {
            //usuario encontrado, revisando contraseña
            for (Usuario user : userList) {
                if (user.getPassword().equals(password) && user.getIsadmin()== true) {
                    //usuario y contraseña valido guardado usuario en session
                    HttpSession session = request.getSession();
                    session.setAttribute("user", user);
                    //redirigiendo a inbox
                    //response.sendRedirect("menuadmin");
                    String view = "view/vistas/menuadmin.jsp";
                    RequestDispatcher rd = request.getRequestDispatcher(view);
                    rd.forward(request, response);
                    
                    pass = true;
                    break;
                } else if (user.getPassword().equals(password)){
                    //usuario y contraseña valido guardado usuario en session
                    HttpSession session = request.getSession();
                    session.setAttribute("user", user);
                    //redirigiendo a inbox
                    //response.sendRedirect("menutecnico");
                    String view = "view/vistas/menutecnico.jsp";
                    RequestDispatcher rd = request.getRequestDispatcher(view);
                    rd.forward(request, response);
                    pass = true;
                    break;
            }
            }
            //usuario encontrado pero contraseña incorrecta
            if (!pass) {
                RequestDispatcher rd = request.getRequestDispatcher("/login.jsp");
                request.setAttribute("error", errorMsg);
                rd.forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}