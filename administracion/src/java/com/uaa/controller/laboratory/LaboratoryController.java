/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.controller.laboratory;

import com.uaa.entity.Laboratorio;
import com.uaa.entity.Usuario;
import com.uaa.helper.JBLaboratoryHelper;
import com.uaa.jpa.exceptions.IllegalOrphanException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lalo
 */
@WebServlet(name = "LaboratoryController", urlPatterns = {"/labcontroller"})
public class LaboratoryController extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String view = "/create_laboratory.jsp";
        RequestDispatcher rd = request.getRequestDispatcher(view);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String view = "/view/vistas/menuadmin.jsp";
        JBLaboratoryHelper laboratoryHelper = JBLaboratoryHelper.getInstance();
        //obtener parametros de la forma
        String action = request.getParameter("action");
        Laboratorio item = getItemFromRequest(request);
        switch (action) {
            case "create":
                String name = request.getParameter("name");
                String ubicacion = request.getParameter("ubicacion");

                //crear una nueva instancia de Usuario para utilizarse en JPA
                Laboratorio lab = new Laboratorio();
                lab.setNombre(name);
                lab.setUbicacion(ubicacion);

                try {
                    laboratoryHelper.registerLaboratory(lab);
                } catch (IllegalOrphanException ex) {
                    //Logger.getLogger(LaboratoryController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "update":
        {
            try {
                laboratoryHelper.update(item);
            } catch (Exception ex) {
                Logger.getLogger(LaboratoryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            break;
            default:
                break;
        }

        //redirigir a la vista create_user.html
        RequestDispatcher rd = request.getRequestDispatcher(view);
        rd.forward(request, response);
    }

    private Laboratorio getItemFromRequest(HttpServletRequest request) {
        Laboratorio item = new Laboratorio();
        String id = request.getParameter("id");
        String nombre = request.getParameter("name");
        String ubicacion = request.getParameter("ubicacion");
        if (id != null) {
            item.setId(Integer.parseInt(id));
        }
        if (nombre != null) {
            item.setNombre(nombre);
        }
        if (ubicacion != null) {
            item.setUbicacion(ubicacion);
        }

        return item;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
