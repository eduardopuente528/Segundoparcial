/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.controller.material;

import com.uaa.entity.Material;
import com.uaa.helper.JBMaterialHelper;
import com.uaa.jpa.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lalo
 */
@WebServlet(name = "MaterialController", urlPatterns = {"/controller"})
public class MaterialController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String view = "/alta_material.jsp";
        RequestDispatcher rd = request.getRequestDispatcher(view);
        rd.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final String view = "/view/vistas/menuadmin.jsp";
        JBMaterialHelper materialHelper = JBMaterialHelper.getInstance();
        //obtener parametros de la forma

        //crear una nueva instancia de Usuario para utilizarse en JPA
        String action = request.getParameter("action");
        Material item = getItemFromRequest(request);
        switch (action) {
            case "alta_material":
                String descripcion = request.getParameter("descripcion");
                int cantidad = Integer.parseInt(request.getParameter("cantidad"));
                String labo = request.getParameter("labo");
                Material mat = new Material();
                mat.setDescripcion(descripcion);
                mat.setCantidad(cantidad);
                mat.setLaboratorio(labo);

                materialHelper.registerMaterial(mat);
                break;
            //case "update":
            //  beanManager.update(item);
            //break;
            case "baja_material":
                int id = Integer.parseInt(request.getParameter("id"));

                try {
                    materialHelper.delete(id);
                } catch (NonexistentEntityException ex) {
                    //Logger.getLogger(MaterialController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            case "update": {
                try {
                    materialHelper.update(item);
                } catch (Exception ex) {
                    Logger.getLogger(MaterialController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            break;

            default:
                break;
                
                
        }
        
        List<Material> todoList = materialHelper.read();
        request.setAttribute("todoList", todoList);
        RequestDispatcher rd = request.getRequestDispatcher("/consul_gral.jsp");
        rd.forward(request, response);

        //redirigir a la vista create_user.html
        //RequestDispatcher rd = request.getRequestDispatcher(view);
        //rd.forward(request, response);

    }

    private Material getItemFromRequest(HttpServletRequest request) {
        Material item = new Material();
        String id = request.getParameter("id");
        String descripcion = request.getParameter("descripcion");
        String cantidad = request.getParameter("cantidad");
        String laboratorio = request.getParameter("labo");
        if (id != null) {
            item.setId(Integer.parseInt(id));
        }
        if (descripcion != null) {
            item.setDescripcion(descripcion);
        }
        if (cantidad != null) {
            item.setCantidad(Integer.parseInt(cantidad));
        }
        if (laboratorio != null) {
            item.setLaboratorio(laboratorio);
        }

        return item;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
