/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.helper;

import com.uaa.entity.Usuario;
import com.uaa.jpa.UsuarioJpaController;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Lalo
 */
public class JBUserHelper {
    private static JBUserHelper instance;
    private final UsuarioJpaController controller;

    private JBUserHelper() {
        EntityManagerFactory emf
                = Persistence.createEntityManagerFactory("administracionPU");
        controller = new UsuarioJpaController(emf);
    }

    public static JBUserHelper getInstance() {
        if (instance == null) {
            instance = new JBUserHelper();
        }
        return instance;
    }

    public void registerUser(Usuario user) {
        controller.create(user);
    }

    public List<Usuario> getUserByUsername(String username) {
        EntityManager em = controller.getEntityManager();
        Query query = em.createNamedQuery("Usuario.findByUsername");
        query.setParameter("username", username);
        List<Usuario> users = query.getResultList();
        return users;
    }
}
