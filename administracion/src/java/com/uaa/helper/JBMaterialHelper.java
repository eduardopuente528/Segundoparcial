/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.helper;

import com.uaa.entity.Material;
import com.uaa.jpa.MaterialJpaController;
import com.uaa.jpa.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Lalo
 */
public class JBMaterialHelper {
    private static JBMaterialHelper instance;
    private final MaterialJpaController controller;
    
    private JBMaterialHelper(){
        EntityManagerFactory emf
                = Persistence.createEntityManagerFactory("administracionPU");
        controller = new MaterialJpaController(emf);
    }
    
    public static JBMaterialHelper getInstance(){
        if(instance == null){
            instance = new JBMaterialHelper();
        }
        return instance;
    }
    
    public void registerMaterial(Material mat){
        controller.create(mat);
    }
    
    public void delete(int id) throws NonexistentEntityException{
            controller.destroy(id);
    }
    
    public void update(Material item) throws Exception {
        controller.edit(item);
    }
    
    public List<Material> read(){
        return controller.findMaterialEntities();
    }
    
    public List<Material> getLabByName(String mate) {
        EntityManager em = controller.getEntityManager();
        Query query = em.createNamedQuery("Material.findByNombre");
        query.setParameter("nombre", mate);
        List<Material> mat = query.getResultList();
        return mat;
    }
}
