/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uaa.helper;

import com.uaa.entity.Laboratorio;
import com.uaa.jpa.LaboratorioJpaController;
import com.uaa.jpa.exceptions.IllegalOrphanException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Lalo
 */
public class JBLaboratoryHelper {
    private static JBLaboratoryHelper instance;
    private final LaboratorioJpaController controller;
    
    private JBLaboratoryHelper(){
        EntityManagerFactory emf
                = Persistence.createEntityManagerFactory("administracionPU");
        controller = new LaboratorioJpaController(emf);
    }
    
    public static JBLaboratoryHelper getInstance(){
        if(instance == null){
            instance = new JBLaboratoryHelper();
        }
        return instance;
    }
    
    public void registerLaboratory(Laboratorio laboratory) throws IllegalOrphanException{
        controller.create(laboratory);
    }
    
    public List<Laboratorio> getLabByName(String labora) {
        EntityManager em = controller.getEntityManager();
        Query query = em.createNamedQuery("Laboratorio.findByNombre");
        query.setParameter("nombre", labora);
        List<Laboratorio> labs = query.getResultList();
        return labs;
    }

    public void update(Laboratorio item) throws Exception {
        controller.edit(item);
    }
    
}
