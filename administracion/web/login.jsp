<%-- 
    Document   : login
    Created on : 18-nov-2017, 12:28:43
    Author     : Mario1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <h1>Login</h1>
        <p>${error}</p><br/>
        <form action="login" method="POST">
            Username: <input name="username" type="text"  /><br/>
            Password: <input name="password" type="password" /><br/>
            <button>Login</button>
        </form>
    </body>
</html>
