<%-- 
    Document   : consul_gral
    Created on : 27/11/2017, 08:46:56 PM
    Author     : Lalo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Lista de Mensajes</h1>

        <c:if test="${not empty todoList}">
            <table>
                <tr>
                    <th>Id</th>
                    <th>Descripcion</th>
                    <th>Cantidad</th>
                    <th>Laboratorio</th>
                </tr>
                <c:forEach var="todoItem" items="${todoList}">
                    <tr>
                        <td>${todoItem.id}</td>
                        <td>${todoItem.descripcion}</td>
                        <td>${todoItem.cantidad}</td>
                        <td>${todoItem.laboratorio}</td>
                        
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </body>
</html>
